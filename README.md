Vagrant Cassandra and Zookeeper Skeleton
========================================

A skeleton for setting up a Vagrant machine with Cassandra and Zookeeper (provisioned
with Puppet)

Usage
-----

1. Copy the `Vagrantfile` and the `vagrant` subdirectory to your local project.
2. By default, it uses a Ubuntu 12.10 vagrant box, but you can customize it by
modifying the `Vagrantfile`.
3. Review the `vagrant/manifest/init.pp` file, checking out the roles definitions.


