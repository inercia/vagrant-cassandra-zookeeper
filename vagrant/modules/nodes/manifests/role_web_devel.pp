#
# Copyright Alvaro Saurin <alvaro.saurin@gmail.com> - 2014
#

################################################################################################

class nodes::role_web_devel {

    include '::nodes::base'
    include '::nodes::devel'
    include '::nodes::mysql'
    include '::nodes::mysql_devel'
    include '::nodes::apache'
}

