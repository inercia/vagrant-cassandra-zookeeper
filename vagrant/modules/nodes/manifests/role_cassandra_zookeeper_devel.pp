#
# Copyright Alvaro Saurin <alvaro.saurin@gmail.com> - 2014
#

################################################################################################

class nodes::role_cassandra_zookeeper_devel {

    include '::nodes::base'
    include '::nodes::devel'
    include '::java'

	class { '::cassandra':
	  cluster_name => 'YourCassandraCluster',
	  seeds        => [ '192.0.2.5', '192.0.2.23', '192.0.2.42', ],
	}
	
    class {'::zookeeper::cluster':
        server_id => '1',
    }
}

