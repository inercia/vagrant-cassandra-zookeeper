#
# Copyright Alvaro Saurin <alvaro.saurin@gmail.com> - 2014
#

################################################################################################

class nodes::base {

	class { '::selinux':
		mode => 'permissive',
	}

	group { "puppet":
		ensure => "present",
	}

	File { owner => 0, group => 0, mode => 0644 }

	file { '/etc/motd':
		content => "\n\nWelcome to a \n${operatingsystem} ${operatingsystemrelease} machine - Default IP:${ipaddress}!\nManaged by Puppet.\n\n"
	}

    exec { 'iptables-flush':
      command => "/sbin/iptables --flush",
    }

	########################################
	# Basic packages
	########################################
	package {[
			'zeromq',
			'pcre',
			'readline',
			'compat-readline5',
			'joe',
			'wget',
		]:
		ensure 		=> 'present',
	}
}


